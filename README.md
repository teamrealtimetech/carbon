# Carbon

## 릴리즈

## 1. Carbon이란?
멀티 서버 환경에서 리소스 분배 및 영역 할당 등, 다양한 서버들이 함께 동작하기 위한 Base Server Platform입니다.

## 2. 라이센스 및 남기는 말

Carbon는 [Apache License 2.0](./LICENSE.txt) 라이센스를 이용합니다, 여러분의 적극적인 이슈, 기능 피드백을 기대합니다.

```
JeongHwan, Park
+821032735003
parkjeonghwan@realtimetech.co.kr
```