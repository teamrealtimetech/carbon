package com.realtimetech.carbon.type;

import java.text.SimpleDateFormat;
import java.util.Date;

public class TypeCaster {
	private final static SimpleDateFormat DATE_FORMAT = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");

	public static String serializeStringValue(Object value) {
		if (value.getClass() == String.class) {
			return (String) value;
		} else if (value.getClass() == Date.class) {
			return DATE_FORMAT.format((Date) value);
		}

		return value.toString();
	}

	public static Object serializeObjectValue(String value) {
		try {
			return DATE_FORMAT.parse(value);
		} catch (Exception e) {
		}

		try {
			if (value.equals("true") || value.equals("false")) {
				return Boolean.parseBoolean(value);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

		try {
			return Integer.parseInt(value);
		} catch (Exception e) {
		}

		try {
			return Long.parseLong(value);
		} catch (Exception e) {
		}

		try {
			return Float.parseFloat(value);
		} catch (Exception e) {
		}

		try {
			return Double.parseDouble(value);
		} catch (Exception e) {
		}

		return value;
	}
}

