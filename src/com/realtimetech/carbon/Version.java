package com.realtimetech.carbon;

public class Version {
	private int majorVersion;
	private int minorVersion;
	private int buildVersion;

	public Version(int majorVersion, int minorVersion, int buildVersion) {
		this.majorVersion = majorVersion;
		this.minorVersion = minorVersion;
		this.buildVersion = buildVersion;
	}

	public int getBuildVersion() {
		return buildVersion;
	}

	public int getMajorVersion() {
		return majorVersion;
	}

	public int getMinorVersion() {
		return minorVersion;
	}

	@Override
	public String toString() {
		return majorVersion + "." + minorVersion + " " + buildVersion;
	}

	public boolean higherThan(Version version) {
		if (this.majorVersion > version.majorVersion) {
			return true;
		}

		if (this.minorVersion > version.minorVersion) {
			return true;
		}

		if (this.buildVersion > version.buildVersion) {
			return true;
		}

		return false;
	}

	public static Version parseVersion(String version) {
		try {
			String[] strings = version.split("\\.");
			String[] subStrings = strings[1].split(" ");

			int majorVersion = Integer.parseInt(strings[0]);
			int minorVersion = Integer.parseInt(subStrings[0]);
			int buildVersion = Integer.parseInt(subStrings[1]);

			return new Version(majorVersion, minorVersion, buildVersion);
		} catch (Exception e) {
			return new Version(0, 0, 0);
		}
	}

	@Override
	public boolean equals(Object obj) {
		if (obj instanceof Version) {
			Version version = (Version) obj;

			return version.buildVersion == this.buildVersion && version.minorVersion == this.minorVersion && version.majorVersion == this.majorVersion;
		}
		return super.equals(obj);
	}
}
