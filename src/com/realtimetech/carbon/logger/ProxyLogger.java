package com.realtimetech.carbon.logger;

import java.io.File;

import com.realtimetech.carbon.Carbon;
import com.realtimetech.carbon.logger.format.LogFormat;
import com.realtimetech.carbon.logger.level.Level;

public class ProxyLogger implements Logger {
	private String title;

	public ProxyLogger(String title) {
		this.title = title;
	}

	@Override
	public LogFormat getFormat() {
		return Carbon.getInstance().getRootLogger().getFormat();
	}

	@Override
	public void setFormat(LogFormat logFormat) {
		// ProxyLogger can't set format
	}

	@Override
	public String getTitle() {
		return title;
	}

	@Override
	public File getDirectory() {
		return Carbon.getInstance().getRootLogger().getDirectory();
	}

	@Override
	public void setDirectory(File directory) {
		// ProxyLogger can't set directory
	}

	@Override
	public Level getThreshold() {
		return Carbon.getInstance().getRootLogger().getThreshold();
	}

	@Override
	public void setThreshold(Level level) {
		// ProxyLogger can't set threshold
	}

	@Override
	public void printLog(Level level, String title, String message, Object... args) {
		Carbon.getInstance().getRootLogger().printLog(level, title, message, args);
	}
}
