package com.realtimetech.carbon.logger;

import java.io.File;

import com.realtimetech.carbon.logger.category.LogCategorisable;
import com.realtimetech.carbon.logger.format.LogFormat;
import com.realtimetech.carbon.logger.level.Level;

public interface Logger {
	public LogFormat getFormat();
	
	public void setFormat(LogFormat logFormat);
	
	public String getTitle();

	public File getDirectory();

	public void setDirectory(File directory);

	public Level getThreshold();

	public void setThreshold(Level level);

	default void log(Level level, LogCategorisable category, String message, Object... args) {
		if (level.isGreaterOrEqual(getThreshold())) {
			printLog(level, category.getLogCategory(), message, args);
		}

		return;
	}

	default void log(Level level, String message, Object... args) {
		if (level.isGreaterOrEqual(getThreshold())) {
			printLog(level, getTitle(), message, args);
		}

		return;
	}

	default void log(Level level, String message) {
		if (level.isGreaterOrEqual(getThreshold())) {
			printLog(level, getTitle(), message);
		}

		return;
	}

	void printLog(Level level, String title, String message, Object... args);
}
