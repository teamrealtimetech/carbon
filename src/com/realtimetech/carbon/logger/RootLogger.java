package com.realtimetech.carbon.logger;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintStream;
import java.text.SimpleDateFormat;
import java.util.Date;

import com.realtimetech.carbon.logger.format.LogFormat;
import com.realtimetech.carbon.logger.level.Level;

public class RootLogger implements Logger {
	private static final SimpleDateFormat LOG_FILE_FORMAT = new SimpleDateFormat("yyyy-MM-dd");

	private LogFormat logFormat;
	private PrintStream logStream;

	private File directory;

	private Level threshold;

	private String title;

	public RootLogger(String title, PrintStream logStream) {
		this.title = title;
		this.logFormat = new LogFormat();
		this.logStream = logStream;
		this.threshold = Level.INFO;
		this.directory = new File("log/");
	}

	@Override
	public LogFormat getFormat() {
		return logFormat;
	}

	@Override
	public void setFormat(LogFormat logFormat) {
		this.logFormat = logFormat;
	}

	@Override
	public String getTitle() {
		return title;
	}

	@Override
	public File getDirectory() {
		return directory;
	}

	@Override
	public void setDirectory(File directory) {
		this.directory = directory;
	}

	@Override
	public Level getThreshold() {
		return threshold;
	}

	@Override
	public void setThreshold(Level level) {
		this.threshold = level;
	}

	@Override
	public void printLog(Level level, String title, String message, Object... args) {
		String log = logFormat.format(level, title, message, args);
		logStream.println(log);

		if (getDirectory() != null) {
			if(!this.directory.exists()) {
				try {
					this.directory.mkdirs();
				} catch (Exception e) {
				}
			}
			
			String logFileName = directory.getAbsolutePath() + File.separatorChar + LOG_FILE_FORMAT.format(new Date()) + ".log";

			FileWriter fw = null;

			try {
				fw = new FileWriter(logFileName, true);
				fw.write(log + "\r\n");
			} catch (IOException e) {
				e.printStackTrace();
			} finally {
				if (fw != null) {
					try {
						fw.close();
					} catch (IOException e) {
						e.printStackTrace();
					}
				}
			}
		}

	}
}
