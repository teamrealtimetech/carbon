package com.realtimetech.carbon.logger.format;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.IllegalFormatException;
import java.util.UnknownFormatConversionException;

import com.realtimetech.carbon.logger.level.Level;

public class LogFormat {
	private static final SimpleDateFormat DATE_FORMAT = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
	private static final String DEFAULT_FORMAT_STRING = new String("[ %s ][ %s ][ %s ]   %s");

	private SimpleDateFormat dateFormat;
	private String formatString;

	public LogFormat() {
		this.dateFormat = DATE_FORMAT;
		this.formatString = DEFAULT_FORMAT_STRING;
	}

	public LogFormat(SimpleDateFormat dateFormat, String formatString) {
		this.dateFormat = dateFormat;
		this.formatString = formatString;
	}

	public SimpleDateFormat getDateFormat() {
		return dateFormat;
	}

	public void setDateFormat(SimpleDateFormat dateFormat) {
		this.dateFormat = dateFormat;
	}

	public String getFormatString() {
		return formatString;
	}

	public void setFormatString(String formatString) {
		this.formatString = formatString;
	}

	public String format(Level level, String title, String message) {
		String timeFormatted = dateFormat.format(new Date(System.currentTimeMillis()));
		String stringFormatted = null;

		try {
			stringFormatted = String.format(formatString, timeFormatted, title, level.toString(), message);
		} catch (UnknownFormatConversionException e) {
			e.printStackTrace();
			stringFormatted = message;
		}

		return stringFormatted;
	}

	public String format(Level level, String title, String message, Object... args) {
		String timeFormatted = dateFormat.format(new Date(System.currentTimeMillis()));
		String stringFormatted = null;

		try {
			stringFormatted = String.format(formatString, timeFormatted, makePrettyString(title, 15), makePrettyString(level.toString(), 8), String.format(message, args));
		} catch (UnknownFormatConversionException e) {
			stringFormatted = message;

			for (Object arg : args) {
				stringFormatted += arg + "";
			}
		} catch (IllegalFormatException e) {
			stringFormatted = message;

			for (Object arg : args) {
				stringFormatted += arg + "";
			}
		}

		return stringFormatted;
	}

	private static String makePrettyString(String string, int size) {
		if (string.length() > size) {
			string = string.substring(0, size);
		} else {
			String emptySpace = "";
			int needSpace = size - string.length();
			for (int i = 0; i < (int) (needSpace / 2); i++) {
				emptySpace += " ";
			}
			String leftEmptySpace = emptySpace;
			String rightEmptySpace = emptySpace;

			if ((leftEmptySpace.length() + rightEmptySpace.length() + string.length()) != size) {
				rightEmptySpace += " ";
			}

			string = leftEmptySpace + string + rightEmptySpace;
		}
		return string;
	}

}
