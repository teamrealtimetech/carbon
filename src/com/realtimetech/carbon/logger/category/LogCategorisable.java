package com.realtimetech.carbon.logger.category;

public interface LogCategorisable {
	public String getLogCategory();
}
