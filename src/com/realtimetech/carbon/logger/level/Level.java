package com.realtimetech.carbon.logger.level;

public enum Level {
	ALL(0, "ALL"),
	DEBUG(1, "DEBUG"),
	INFO(2, "INFO"),
	WARN(3, "WARNING"),
	ERROR(4, "ERROR"),
	FATAL(5, "FATAL"),
	OFF(100, "OFF");

	private int level;
	private String title;

	Level(int level, String levelString) {
		this.level = level;
		this.title = levelString;
	}

	public boolean isGreaterOrEqual(Level level) {
		return this.level >= level.level;
	}

	public String toString() {
		return title;
	}

	public int toInt() {
		return level;
	}
}
