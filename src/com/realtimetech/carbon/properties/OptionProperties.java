package com.realtimetech.carbon.properties;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.nio.charset.Charset;
import java.util.Stack;

import com.realtimetech.carbon.type.TypeCaster;

public class OptionProperties {
	private SortedProperties properties;
	private File path;

	private Stack<String> keyStack;

	private Charset charset;

	public OptionProperties(File path, Charset charset) {
		this.path = path;
		this.properties = new SortedProperties();
		this.keyStack = new Stack<String>();

		this.charset = charset;

		if (path.getParentFile() != null)
			path.getParentFile().mkdirs();

		if (!path.exists()) {
			trySave();
		} else {
			tryLoad();
		}
	}

	public OptionProperties(File path) {
		this(path, Charset.defaultCharset());
	}

	public void tryLoad() {
		try {
			FileInputStream fileInputStream = new FileInputStream(path);
			properties.load(new InputStreamReader(fileInputStream, charset));
			fileInputStream.close();
		} catch (IOException e) {
		}
	}

	public void trySave() {
		try {
			FileOutputStream fileOutputStream = new FileOutputStream(path);
			properties.store(new OutputStreamWriter(fileOutputStream, charset), null);
			fileOutputStream.close();
		} catch (IOException e) {
		}
	}

	public File getPath() {
		return path;
	}

	public void push(String key) {
		this.keyStack.push(key.toLowerCase());
	}

	public void pop() {
		this.keyStack.pop();
	}

	public int get(String key, int defaultValue) {
		return (int) get(key, (Integer) defaultValue);
	}

	public String get(String key, String defaultValue) {
		return TypeCaster.serializeStringValue(get(key, (Object) defaultValue));
	}

	public Object get(String key, Object defaultValue) {
		String createdKey = "";

		for (String domains : this.keyStack) {
			createdKey = createdKey + domains + ".";
		}

		createdKey = createdKey + key;

		if (!properties.containsKey(createdKey)) {
			properties.setProperty(createdKey, TypeCaster.serializeStringValue(defaultValue));
		}

		return TypeCaster.serializeObjectValue((String) properties.getOrDefault(createdKey, defaultValue));
	}
}