package com.realtimetech.carbon.service;

import com.realtimetech.carbon.Carbon;
import com.realtimetech.carbon.logger.level.Level;
import com.realtimetech.carbon.server.Server;

public abstract class Service extends Thread {
	private boolean working;
	private Server server;

	public Server getServer() {
		return server;
	}

	public void bind(Server server) {
		this.server = server;
		this.working = false;
		this.setDaemon(true);
		this.setName(server.getName() + "-" + getProviderName() + "-Service");
		
		this.onBind();
		Carbon.getInstance().getLogger(server.getClass()).log(Level.INFO, "Bind '%s' service in '%s' server.", getName(), server.getName());
		
		this.start();
	}

	@Override
	public void run() {
		this.working = true;

		while (this.working) {
			try {
				onService();
			} catch (InterruptedException e) {
			}
		}

		this.working = false;
	}

	public boolean isWorking() {
		return working;
	}

	public void unbind() {
		this.onUnbind();
		Carbon.getInstance().getLogger(server.getClass()).log(Level.INFO, "Unbind '%s' service in '%s' server.", getName(), server.getName());
		
		this.working = false;
		this.interrupt();
	}

	abstract public String getProviderName();

	abstract public void onBind();
	
	abstract public void onService() throws InterruptedException;

	abstract public void onUnbind();
}
