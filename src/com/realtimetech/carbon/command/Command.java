package com.realtimetech.carbon.command;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

public abstract class Command {
	private List<Command> childCommands;

	public Command() {
		this.childCommands = new LinkedList<Command>();
	}

	public void addCommand(Command command) {
		this.childCommands.add(command);
	}

	public void routeCommand(String[] command, int index) {
		boolean needProcess = true;

		if (command.length > index) {
			for (Command childCommand : childCommands) {
				if (command[index].equalsIgnoreCase(childCommand.getCommand())) {
					needProcess = false;

					childCommand.routeCommand(command, index + 1);
					break;
				}
			}
		}

		if (needProcess) {
			onCommand(Arrays.copyOfRange(command, index, command.length));
		}
	}

	public abstract String getCommand();

	public abstract void onCommand(String[] arguments);
}
