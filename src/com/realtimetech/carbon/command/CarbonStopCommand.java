package com.realtimetech.carbon.command;

import com.realtimetech.carbon.Carbon;

public class CarbonStopCommand extends Command{
	@Override
	public String getCommand() {
		return "stop";
	}

	@Override
	public void onCommand(String[] arguments) {
		Carbon.getInstance().stop();
	}
}
