package com.realtimetech.carbon.command;

import java.util.Scanner;

import com.realtimetech.carbon.service.Service;

public class CommandService extends Service {
	private Scanner scanner;
	private Command rootCommand;

	public CommandService() {
		this.rootCommand = new Command() {
			@Override
			public void onCommand(String[] arguments) {
				System.out.println("Unknown command, please check command again.");
			}

			@Override
			public String getCommand() {
				return "";
			}
		};
	}

	public Command getRootCommand() {
		return rootCommand;
	}

	@Override
	public String getProviderName() {
		return "CommandProcessor";
	}

	@Override
	public void onBind() {
		this.scanner = new Scanner(System.in);

	}

	@Override
	public void onService() throws InterruptedException {
		String command = this.scanner.nextLine();

		this.rootCommand.routeCommand(command.split(" "), 0);
	}

	@Override
	public void onUnbind() {
		this.scanner.close();
	}

}
