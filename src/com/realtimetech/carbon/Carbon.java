package com.realtimetech.carbon;

import java.util.Collections;
import java.util.Hashtable;
import java.util.LinkedList;
import java.util.concurrent.ConcurrentLinkedQueue;

import com.realtimetech.carbon.command.CarbonStopCommand;
import com.realtimetech.carbon.command.Command;
import com.realtimetech.carbon.command.CommandService;
import com.realtimetech.carbon.logger.Logger;
import com.realtimetech.carbon.logger.ProxyLogger;
import com.realtimetech.carbon.logger.RootLogger;
import com.realtimetech.carbon.properties.OptionProperties;
import com.realtimetech.carbon.reflect.UnsafeAllocator;
import com.realtimetech.carbon.server.Server;
import com.realtimetech.carbon.server.status.ServerStatus;
import com.realtimetech.carbon.task.WrapperTask;

public class Carbon {
	private static Carbon instance;
	private static UnsafeAllocator unsafeAllocator = UnsafeAllocator.create();

	public static UnsafeAllocator getUnsafeAllocator() {
		return unsafeAllocator;
	}

	public static Carbon getInstance() {
		if (instance == null)
			instance = new Carbon();

		return instance;
	}

	private LinkedList<Server> servers;
	private OptionProperties optionProperties;

	private RootLogger rootLogger;
	private Hashtable<String, Logger> loggerMap;

	private ConcurrentLinkedQueue<WrapperTask> taskQueue;
	private LinkedList<WrapperTask> relaunchTaskQueue;

	private Thread taskThread;

	private CommandService commandService;

	private boolean working;

	public Carbon() {
		this.working = false;
		this.servers = new LinkedList<Server>();
		this.loggerMap = new Hashtable<String, Logger>();

		this.taskQueue = new ConcurrentLinkedQueue<WrapperTask>();
		this.relaunchTaskQueue = new LinkedList<WrapperTask>();

		this.commandService = new CommandService();
		this.commandService.getRootCommand().addCommand(new CarbonStopCommand());
	}

	public ConcurrentLinkedQueue<WrapperTask> getTaskQueue() {
		return taskQueue;
	}

	public Thread getTaskThread() {
		return taskThread;
	}

	public RootLogger getRootLogger() {
		return rootLogger;
	}

	public Logger getLogger(Class<? extends Server> clazz) {
		synchronized (loggerMap) {
			return loggerMap.get(clazz.getName());
		}
	}

	public Logger registerLogger(Server server) {
		synchronized (loggerMap) {
			Class<? extends Server> serverClass = server.getClass();
			if (!loggerMap.containsKey(serverClass.getName())) {
				if (loggerMap.isEmpty()) {
					RootLogger rootLogger = new RootLogger(server.getName(), System.out);
					this.rootLogger = rootLogger;
					loggerMap.put(serverClass.getName(), rootLogger);
				} else {
					loggerMap.put(serverClass.getName(), new ProxyLogger(server.getName()));
				}
			}

			return loggerMap.get(serverClass.getName());
		}
	}

	public LinkedList<Server> getServers() {
		return servers;
	}

	public void registerServer(Server server) {
		this.registerLogger(server);
		this.servers.add(server);
		this.commandService.getRootCommand().addCommand(server.getCommand());
	}

	public boolean isMainServer(Server server) {
		return this.servers.indexOf(server) == 0;
	}

	public Server getMainServer() {
		return getServers().get(0);
	}

	public OptionProperties getOptionProperties() {
		return optionProperties;
	}

	public void setOptionProperties(OptionProperties optionProperties) {
		this.optionProperties = optionProperties;
	}

	public void start() {
		if (getMainServer().getStatus() == ServerStatus.READY) {
			getMainServer().start();
			return;
		}

		getMainServer().bindService(this.commandService);
		this.working = true;
		while (isWorking()) {
			relaunchTaskQueue.clear();

			Long startProcessTime = System.currentTimeMillis();
			Long minimumWaitTime = Long.MAX_VALUE;

			while (!taskQueue.isEmpty()) {
				WrapperTask task = taskQueue.poll();

				long waitTime = task.tryRun(startProcessTime);
				if (waitTime < 0) {
					if (task.isLoop() && !task.isDone()) {
						relaunchTaskQueue.add(task);
						minimumWaitTime = task.getWrappedTask().getWaitMilliseconds();
					}
				} else {
					minimumWaitTime = Math.min(minimumWaitTime, waitTime);
					relaunchTaskQueue.add(task);
				}
			}

			for (WrapperTask task : relaunchTaskQueue) {
				taskQueue.offer(task);
			}

			try {
				Thread.sleep(minimumWaitTime);
			} catch (InterruptedException e) {
			}
		}
	}

	public synchronized void stop() {
		LinkedList<Server> reversedServer = new LinkedList<Server>(servers);
		
		Collections.reverse(reversedServer);
		
		for (Server server : reversedServer) {
			if(server.getStatus() == ServerStatus.RUNNING) {
				server.stop();
			}
		}
		
		this.working = false;
		this.taskThread.interrupt();
	}

	public synchronized boolean isWorking() {
		return working;
	}

	@SuppressWarnings("unchecked")
	public static void main(String[] args) {
		if (args.length >= 1) {
			try {
				Class<?> clazz = Class.forName(args[0]);

				if (Server.class.isAssignableFrom(clazz)) {
					Class<? extends Server> serverClass = (Class<? extends Server>) clazz;
					Server.createServer(serverClass);
				} else {

				}

				Carbon.getInstance().taskThread = Thread.currentThread();
				Carbon.getInstance().taskThread.setName("Carbon-Work-Thread");
				Carbon.getInstance().start();

			} catch (ClassNotFoundException e) {
				e.printStackTrace();
			} catch (Exception e) {
				e.printStackTrace();
			}

		} else {
		}

	}
}
