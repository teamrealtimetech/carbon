package com.realtimetech.carbon.task.runnable;

public abstract class Task {
	private boolean done;

	public Task() {
		this.done = false;
	}
	
	public boolean isDone() {
		return !isLoop() || done;
	}

	public void done() {
		this.done = true;
	}

	public abstract boolean isLoop();

	public abstract long getWaitMilliseconds();

	public abstract void run();
}
