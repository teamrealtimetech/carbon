package com.realtimetech.carbon.task.runnable;

public abstract class OnceTask extends OnceLaterTask {
	@Override
	public long getWaitTicks() {
		return 0;
	}

	public abstract void run();
}
