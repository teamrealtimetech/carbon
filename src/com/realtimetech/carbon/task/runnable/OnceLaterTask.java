package com.realtimetech.carbon.task.runnable;

public abstract class OnceLaterTask extends Task {
	@Override
	public boolean isLoop() {
		return false;
	}

	public abstract long getWaitTicks();

	public abstract void run();
}
