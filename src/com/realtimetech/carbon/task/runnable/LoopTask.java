package com.realtimetech.carbon.task.runnable;

public abstract class LoopTask extends Task {
	@Override
	public boolean isLoop() {
		return true;
	}

	@Override
	public long getWaitMilliseconds() {
		return getIntervalMilliseconds();
	}

	public abstract long getIntervalMilliseconds();
	public abstract void run();
}
