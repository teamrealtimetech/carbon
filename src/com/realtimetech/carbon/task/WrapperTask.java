package com.realtimetech.carbon.task;

import com.realtimetech.carbon.task.runnable.Task;

public class WrapperTask {
	private Task wrappedTask;
	private long lastRunTime;

	public WrapperTask(Task wrappedTask) {
		this.wrappedTask = wrappedTask;

		this.lastRunTime = System.currentTimeMillis() + wrappedTask.getWaitMilliseconds();
	}

	public boolean isDone() {
		return wrappedTask.isDone();
	}

	public boolean isLoop() {
		return wrappedTask.isLoop();
	}

	public Task getWrappedTask() {
		return wrappedTask;
	}
	
	public long tryRun(long currentTime) {
		if (this.lastRunTime <= currentTime) {
			this.wrappedTask.run();

			this.lastRunTime = currentTime + wrappedTask.getWaitMilliseconds();
			return -1;
		}

		return this.lastRunTime - currentTime;
	}
}
