package com.realtimetech.carbon.server.command;

import com.realtimetech.carbon.command.Command;
import com.realtimetech.carbon.server.Server;

public class ServerVersionCommand extends Command {
	private Server server;

	public ServerVersionCommand(Server server) {
		this.server = server;
	}

	@Override
	public String getCommand() {
		return server.getName();
	}

	@Override
	public void onCommand(String[] arguments) {
		System.out.println(server.getNameWithVersion());
	}
}
