package com.realtimetech.carbon.server.command;

import com.realtimetech.carbon.command.Command;
import com.realtimetech.carbon.server.Server;

public class ServerStopCommand extends Command {
	private Server server;

	public ServerStopCommand(Server server) {
		this.server = server;
	}

	@Override
	public String getCommand() {
		return "stop";
	}

	@Override
	public void onCommand(String[] arguments) {
		server.stop();
	}
}
