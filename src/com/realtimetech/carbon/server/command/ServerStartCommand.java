package com.realtimetech.carbon.server.command;

import com.realtimetech.carbon.command.Command;
import com.realtimetech.carbon.server.Server;

public class ServerStartCommand extends Command {
	private Server server;

	public ServerStartCommand(Server server) {
		this.server = server;
	}

	@Override
	public String getCommand() {
		return "start";
	}

	@Override
	public void onCommand(String[] arguments) {
		server.start();
	}
}
