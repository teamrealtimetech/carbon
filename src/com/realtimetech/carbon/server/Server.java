package com.realtimetech.carbon.server;

import java.io.File;
import java.util.LinkedList;
import java.util.List;

import com.realtimetech.carbon.Carbon;
import com.realtimetech.carbon.Version;
import com.realtimetech.carbon.command.Command;
import com.realtimetech.carbon.logger.level.Level;
import com.realtimetech.carbon.properties.OptionProperties;
import com.realtimetech.carbon.server.command.ServerStartCommand;
import com.realtimetech.carbon.server.command.ServerStopCommand;
import com.realtimetech.carbon.server.command.ServerVersionCommand;
import com.realtimetech.carbon.server.status.ServerStatus;
import com.realtimetech.carbon.service.Service;
import com.realtimetech.carbon.task.WrapperTask;
import com.realtimetech.carbon.task.runnable.OnceTask;
import com.realtimetech.carbon.task.runnable.Task;

public abstract class Server {
	private ServerStatus status;

	private List<Service> services;

	private Command command;
	private Version version;
	
	public Server() {
		this.status = ServerStatus.NOT_READY;
		this.services = new LinkedList<Service>();

		this.command = new ServerVersionCommand(this);
		
		this.command.addCommand(new ServerStartCommand(this));
		this.command.addCommand(new ServerStopCommand(this));

		Carbon.getInstance().registerServer(this);
	}

	public Version getVersion() {
		if(this.version == null) {
			this.version = createVersion();
		}
		
		return this.version;
	}
	
	public abstract Version createVersion();

	public abstract String getName();

	public String getNameWithVersion() {
		return getName() + " " + getVersion();
	}

	public Command getCommand() {
		return command;
	}

	public void addCommand(Command command) {
		this.command.addCommand(command);
	}

	protected abstract String getPropertiesFileName();

	protected abstract boolean onInitialize();

	protected abstract boolean onStart();

	protected abstract boolean onStop();

	protected abstract Server newServerInstance(OptionProperties optionProperties);

	protected Server newServerInstance() {
		Carbon carbon = Carbon.getInstance();

		if (carbon.getOptionProperties() == null) {
			File propertiesFile = new File(getPropertiesFileName());
			carbon.setOptionProperties(new OptionProperties(propertiesFile));
		}

		carbon.getOptionProperties().push(getName());
		Server newServerInstance = newServerInstance(carbon.getOptionProperties());
		carbon.getOptionProperties().pop();

		carbon.getOptionProperties().trySave();

		return newServerInstance;
	}

	public static Server createServer(Class<? extends Server> targetServerClass) {
		try {
			Server newServerInstance = Carbon.getUnsafeAllocator().newInstance(targetServerClass).newServerInstance();

			newServerInstance.initialize();

			return newServerInstance;
		} catch (Exception e) {
			e.printStackTrace();
		}

		return null;
	}

	public ServerStatus getStatus() {
		return status;
	}

	public List<Service> getServices() {
		return services;
	}

	public boolean initialize() {
		if (this.status == ServerStatus.NOT_READY) {
			Carbon.getInstance().getLogger(this.getClass()).log(Level.INFO, "Initializing '%s' server..", getNameWithVersion());
			if (onInitialize()) {
				this.status = ServerStatus.READY;
				Carbon.getInstance().getLogger(this.getClass()).log(Level.INFO, "'%s' server has been initialized.", getNameWithVersion());

				return true;
			}
		}

		Carbon.getInstance().getLogger(this.getClass()).log(Level.INFO, "Failed to '%s' server initalize.", getNameWithVersion());
		return false;
	}

	public boolean start() {
		if (this.status == ServerStatus.NOT_READY) {
			if (!initialize()) {
				return false;
			}
		}

		Carbon.getInstance().getLogger(this.getClass()).log(Level.INFO, "Starting '%s' server..", getNameWithVersion());
		if (this.status == ServerStatus.READY && onStart()) {
			this.status = ServerStatus.RUNNING;
			Carbon.getInstance().getLogger(this.getClass()).log(Level.INFO, "'%s' server has been started.", getNameWithVersion());

			if (Carbon.getInstance().getMainServer() == this) {
				Carbon.getInstance().start();
			}
			return true;
		}

		Carbon.getInstance().getLogger(this.getClass()).log(Level.INFO, "Failed to '%s' server start.", getNameWithVersion());
		return false;
	}

	public boolean stop() {
		if (this.status == ServerStatus.RUNNING) {
			this.status = ServerStatus.CLOSING;

			for (Service service : services) {
				if (service.isWorking()) {
					service.unbind();
				}
			}

			Carbon.getInstance().getLogger(this.getClass()).log(Level.INFO, "Stopping '%s' server..", getNameWithVersion());
			if (onStop()) {
				this.status = ServerStatus.CLOSED;
				Carbon.getInstance().getLogger(this.getClass()).log(Level.INFO, "'%s' server has been stoped.", getNameWithVersion());

				this.addTask(new OnceTask() {
					@Override
					public long getWaitMilliseconds() {
						return 1000;
					}

					@Override
					public void run() {
						boolean alivedServer = false;

						for (Server server : Carbon.getInstance().getServers()) {
							if (server.status != ServerStatus.CLOSED) {
								alivedServer = true;
							}
						}

						if (!alivedServer) {
							Carbon.getInstance().stop();
						}
					}
				});

				return true;
			}
		}

		Carbon.getInstance().getLogger(this.getClass()).log(Level.INFO, "Failed to '%s' server stop.", getNameWithVersion());

		return false;
	}

	public void addTask(Task task) {
		Carbon.getInstance().getTaskQueue().offer(new WrapperTask(task));
		Carbon.getInstance().getTaskThread().interrupt();
	}

	public boolean bindService(Service service) {
		if (!services.contains(service)) {
			services.add(service);

			service.bind(this);

			return true;
		}

		return false;
	}

	public boolean unbindService(Service service) {
		if (services.contains(service)) {
			services.remove(service);

			service.unbind();

			return true;
		}

		return false;
	}
}
