package com.realtimetech.carbon.server.status;

public enum ServerStatus {
	NOT_READY, READY, RUNNING, CLOSING, CLOSED;
}
